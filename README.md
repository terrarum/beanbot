# BeanBot

A slack bot that currently does one useless thing and zero useful things.

## Features

- Randomly replaces your status emoji every ten seconds

## Instructions

1. Check out this repo
2. Run `npm install`
3. Run `npm run build`
4. https://api.slack.com/apps/
5. Create New App - give name, choose workspace
6. Under 'Add features and functionality' click 'Permissions'
7. Under 'User Token Scopes' click 'Add an OAuth Scope'
8. Add `emoji:read` and `users.profile:write`
9. Click 'Install to Workspace' and follow the instructions
10. Copy your User OAuth Token
11. Run `USER_TOKEN=<your token> npm run start`

Congration, you done it!

## Comments

The default time is ten seconds. The shortest interval you can have is six seconds before you get [rate limited](https://api.slack.com/methods/users.profile.set).

The bot is set up in development mode, so you just run it from your machine or whatever and it can only modify your user because you're using a User OAuth Token which is associated with your Slack account on the workspace you installed it to.

# To Do

- Run this on the internet as a proper slack bot I guess
- Add more useless features
- Add useful features???
