import { WebClient } from '@slack/web-api';

import gatherEmoji from './gatherEmoji';

const USER_TOKEN = process.env.USER_TOKEN;

async function init() {
    const bot = new WebClient(USER_TOKEN);

    const emojiList = await gatherEmoji(bot);

    setInterval(async () => {
        const emoji = getRandomEmoji(emojiList);
        await setEmoji(bot, emoji);
    }, 10000);
}

async function setEmoji(bot: WebClient, emoji: string) {
    console.log(`Setting to :${emoji}:`);

    const profile = { status_emoji: `:${emoji}:` };

    await bot.users.profile.set({ profile: JSON.stringify(profile) });
}

function getRandomEmoji(emoji: string[]): string {
    return emoji[Math.floor(Math.random() * emoji.length)];
}

init().then();
