import { WebClient } from '@slack/web-api';
import iamcalEmoji from 'emojibase-data/en/shortcodes/iamcal.json';

async function getCustomEmoji(bot: WebClient): Promise<string[]> {
    const customEmoji = await bot.emoji.list();

    if (!customEmoji.ok) {
        throw new Error('Emoji bad');
    }

    return Object.keys(customEmoji.emoji);
}

function getSlackEmoji(emojiObject: typeof iamcalEmoji): string[] {
    const shortcodes: string[] = [];

    Object.values(emojiObject).forEach(item => {
        if (typeof item === 'string') {
            shortcodes.push(item);
        }

        if (typeof item === 'object') {
            item.forEach(subItem => {
                if (typeof subItem === 'string') {
                    shortcodes.push(subItem)
                }
            })
        }
    });

    return shortcodes;
}

function combineEmoji(defaultEmoji: string[], customEmoji: string[]): string[] {
    console.log(`${defaultEmoji.length} default emoji, ${customEmoji.length} custom emoji.`)
    const emojiNames: string[] = [];

    defaultEmoji.forEach(item => emojiNames.push(item));
    customEmoji.forEach(item => emojiNames.push(item));

    return emojiNames;
}

export default async function gatherEmoji(bot: WebClient): Promise<string[]> {
    const customEmoji = await getCustomEmoji(bot);
    const slackEmoji = getSlackEmoji(iamcalEmoji);

    return combineEmoji(slackEmoji, customEmoji);
}
